# Atelier ligne de commande dans un environment GNU/linux

Plan / résumé de l'atelier du 19/12/2017 - 20h~22h

### Notes

 - certaines de ces informations sont spécifiques à Debian, 
   beaucoup sont spécifiques à Linux, et plupart sont applicables
   aux systèmes basés sur UNIX.
 - certaines commandes sont à exécuter en tant qu'administrateur (avec `sudo`), 
   d'autres ne montreront pas les mêmes résultats selon l'utilisateur qui les 
   exécute (`top`, `htop`)

## Diagnostiquer le système

### Messages du noyau

Le noyau est un composant central du système, qui gère les relations 
entre les périphériques et "l'espace utilisateur" (nos applications). 
Il peut être utile de regarder ses journaux, pour diagnostiquer un 
problème avec un périphérique.

Cela peut être fait d'au moins deux manières :
 - avec la commande `dmesg -wt` (voir `dmesg --help`)
   (`-t` pour pour un temps lisible et `-w` un affichage en temps réel)
 - avec la commande `tail -f /var/log/kern.log`
   ("kernel" veut dire noyau en anglais, `tail` affichage la fin d'un fichier, `-f` en temps réel)

Ces journaux peuvent, par exemple, nous apprendre que nous ne pouvons 
nous connecter à un réseau Wi-Fi car les "firmwares" manquent, ou encore
que la coupure physique de la radio est activée.

### Etat des processus, du processeur, de la mémoire

Si le système devient lent ou ne répond plus, cela peut être lié à un problème de 
ressources manquantes, par exemple à cause d'un programme qui en consomme trop.

Les applications `top` (et sa version plus évoluée `htop`) permettent d'avoir accès 
à un moniteur système en terminal. La commande `ps` permet d'afficher la liste des 
processus (voir `man ps`)

## Gérer le système

### Langages activés

Les langues sont activées par des "localisations" ("locale") en anglais, 
qui fournissent tout un ensemble de donnée utilise pour l'internationalisation 
de l'interface (langue, formattage de la date de l'heure, encodage).

La locale de base est appelée "C", et utilise en encodage ASCII, 
non adapté à nos caractères accentués.

Un certain nombre de ces "locales" sont activés, et une de ces "locales" est configurée 
comme "locale par défaut". Plusieurs applications (dont le gestionnaire de session), 
permettent ensuite de choisir parmi ces "locales".

Il existe plusieurs "locales" applicables au français :
 - `fr_BE ISO-8859-1` (Belgique, en encodage Latin-1)
 - `fr_BE.UTF-8 UTF-8` (Belgique, en encodage UTF-8)
 - `fr_BE@euro ISO-8859-15` (Belgique, en encodage Latin-9, fournit le symbole €)
 - `fr_CA ISO-8859-1` (Canada, en encodage Latin-1)
 - `fr_CA.UTF-8 UTF-8` (Canada, en encodage UTF-8)
 - `fr_CH ISO-8859-1` (Suisse, en encodage Latin-1)
 - `fr_CH.UTF-8 UTF-8` (Suisse, en encodage UTF-8)
 - `fr_FR ISO-8859-1` (France, en encodage Latin-1)
 - `fr_FR.UTF-8 UTF-8` (France, en encodage UTF-8)
 - `fr_FR@euro ISO-8859-15` (France, en encodage Latin-9, fournit le symbole €)

On peut observer qu'il y a plusieurs "locales" pour chaque pays francophone, 
déclinées en fonction de l'encodage utilisé :
 - `ISO-8859-1` est une table de codage historique, aussi connue comme `Latin-1` ou `CP819`, 
   à la base de beaucoup d'autre codages (dont UTF-8), utilisée comme table de codage par 
   défaut sur la plupart des systèmes, en l'absence d'autre configuration.
 - `ISO-8859-15` est une extension de cette table, qui fournit notamment le symbole `€`
 - `UTF-8` est un encodage moderne, à taille variable, permettant de représenter tous les 
   caractères Unicode (beaucoup).

Le choix recommandé pour des nouvelles installations est d'utiliser `UTF-8`, 
donc, en France, `fr_FR.UTF-8`. Cela détermine aussi l'encodage par défaut 
des documents texte qui seront produits sur cet ordinateur.

Sous Debian, les "locales" sont choisies en demandant la 
reconfiguration du paquet qui les fournit, en exécutant
`dpkg-reconfigure locales` en tant qu'administrateur.

Les "locales" peuvent aussi être configurées dans le fichier `/etc/locale.gen`, 
en commentant/décommentant les lignes appropriées.

### Systèmes de fichiers

- fichiers/dossiers cachés préfixés par un "." ( `.hidden`)
- afficher les points de montages avec `mount`
- utilisation des disques (`df -h`, `du -hd1`)
- rejouer la commande précédente avec sudo `sudo !!`

### Système de paquets

- `apt` sur Debian
- reconfigurer les paquets avec `dpkg-reconfigure`
   
