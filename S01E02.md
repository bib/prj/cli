# Atelier ligne de commande dans un environment GNU/linux

Plan / résumé de l'atelier du 05/12/2017 - 20h~22h

## La base

Le shell, kézaco?

### Syntaxe de base

    <commande> [options] [arguments]

### Quelques commandes basiques

  - `man` : afficher le manuel de chaque commande
  - `cd` : se déplacer dans une arborescence
  - `pwd` : connaître le chemin courant (répertoire dans lequel on est)
  - `ls` : lister le contenu d'un répertoire (fichiers et répertoire)
  - `cp` : copier un fichier / répertoire
  - `mv` : déplacer/renommer les fichiers / répertoires
  - `rm` : effacer (attention : pas de poubelle!)
  - `mkdir` : créer un répertoire
  - `cat` : afficher le contenu d'un fichier


### Les intepréteurs de commande

SH / BASH (Brian Fox, publié en 1988 pour la FSF) / ZSH (1990) / FISH

### Manipulation du terminal

  - abusage de l'autocomplétion (touche `Tab`)
  - rejouer les commandes (`flèches haut/bas`)
  - afficher l'historique avec la commande `history`
  - rechercher une entrée dans l'historique (`CTRL+R`)
  - Aller en début de ligne (`CTRL+A`)
  - Aller à la fin de la ligne (`CTRL+E`)
  - Effacer le mot courant (`CTRL+W`)
  - Effacer la ligne courante (`CTRL+U`)
  - Effacer le texte de la ligne après le curseur (`CTRL+K`)
  - inverser les deux caractères vers le curseur (`CTRL+T`)

## Jouons!

### Manipulation de base

  - Parcourir les pages d'aide dans `man`
  - Créer des répertoires et se déplacer dans l'arborescence (`mkdir`, `cd`, `tree`)
  - Supprimer des fichiers avec `rm`
  - chercher / filtrer avec `grep`
  - compter les élémets avec `wc`

### Un peu plus loin…

  - enchaînements de commande (séparées par `;`) ou avec test du retour (`&&`)
  - les entrées/sorties (`STDIN` / `STDOUT` / `STDERR`)
  - les redirections  ( `>`, `<`)
  - le pipe (`|`) : redirection de la sortie standard vers l’entrée standard

### La coquille en script

  - écriture d'un simple script bash (=> la semaine prochaine)

Voir le script `welcome.sh` pour exemple.

## Liens

  - https://fr.wikipedia.org/wiki/Interface_syst%C3%A8me
  - http://people.cryst.bbk.ac.uk/%7Etickle/notes/unix-beg.html
